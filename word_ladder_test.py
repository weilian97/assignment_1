import unittest
import word_ladder
from word_ladder import *

##function use to create same length dictionary
def same_length_dictionary(x):
    same_length_dict = []
    for word in words:  ##loop every item in the dictionary
        if len(word) == len(
                x):  ##every word which has same letter length with the"start" word will be appended into a list
            same_length_dict.append(word)
    return same_length_dict

class Test_Word_Ladder(unittest.TestCase):

     def test_start_word_check(self):
         self.assertEqual(word_ladder.check_start_word("lead",same_length_dictionary("lead")), "lead")    ##test if the return value equal to expected value ("lead")
     def test_target_word_check(self):

         self.assertEqual(word_ladder.check_target_word("gold",same_length_dictionary("lead"),"lead"), "gold") ##test if the return value equal to expected value ("gold")

     def test_check_absent_word(self):
         self.assertEqual(word_ladder.check_absent_word(["hate","late","duck"],"lead","gold",same_length_dictionary("lead")),["hate","late","duck"]) ##test if the return value equal to expected value (["hate","late","duck")

     def test_intermediatry_word(self):
         self.assertFalse(word_ladder.check_intermediary_word("n","lead", "gold",same_length_dictionary("lead"),["hate","late","duck"] ))  ##test if it return False if input "n" as first parameter which is user reply  "no"


     def test_find(self):   ##test if the return value equal to expected value (['bead', 'dead', 'head', 'mead', 'read', 'load', 'lend', 'leud', 'lewd', 'leaf', 'leak', 'leal', 'lean', 'leap', 'lear', 'leas'])
         self.assertEqual(word_ladder.find("lead",same_length_dictionary("lead"), {"lead": True}), ['bead', 'dead', 'head', 'mead', 'read', 'load', 'lend', 'leud', 'lewd', 'leaf', 'leak', 'leal', 'lean', 'leap', 'lear', 'leas'])

     def test_build(self):
        self.assertTrue(word_ladder.build(".ead", same_length_dictionary("lead"), {"lead": True},[]))   ##test if it return True if there is any value return
        self.assertEqual(word_ladder.build(".ead", same_length_dictionary("lead"), {"lead": True}, []),['bead', 'dead', 'head', 'mead', 'read'])##test if the return value equal to expected value (['bead', 'dead', 'head', 'mead', 'read'])
        self.assertEqual(word_ladder.build("l.ad", same_length_dictionary("lead"), {"lead": True}, []),['load'])##test if the return value equal to expected value (['load'])
        self.assertEqual(word_ladder.build("le.d", same_length_dictionary("lead"), {"lead": True}, []), ['lend', 'leud', 'lewd']) ##test if the return value equal to expected value (['lend', 'leud', 'lewd'])
        self.assertEqual(word_ladder.build("lea.", same_length_dictionary("lead"), {"lead": True}, []),['leaf', 'leak', 'leal', 'lean', 'leap', 'lear', 'leas']) ##test if the return value equal to expected value (['leaf', 'leak', 'leal', 'lean', 'leap', 'lear', 'leas'])

     def test_bfs(self):
         self.assertFalse(word_ladder.bfs("zebra","smart",same_length_dictionary("lead"),{"zebra":True})) ##test if the return False if there is no path found
         self.assertEqual(word_ladder.bfs("lead", "gold", same_length_dictionary("lead"), {"zebra": True}),["lead","load","goad","gold"]) ##test if the return value equal to expected value (["lead","load","goad","gold"])
if __name__ == '__main__':
    unittest.main()