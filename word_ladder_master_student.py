import re
from collections import deque
#function which check the entered file name if it is valid within the directory
def file_check_error():
  while True:
    try:
      filename = input("Enter dictionary name: ")
      return open(filename)
    except FileNotFoundError:
      print ("\nUnable to locate this file. Please try again. (e.g dictionary.txt)\n")

##function that check the start word to make sure it is non-digit/ word in the dictionary

def check_start_word(words):
  while True:                                                     ##loop when user enter something
    word= input("Enter start word: ").replace(" ","")             ##ignore the blank space that user enter

    if word in words and len(word)> 2:                            ##to ensure the word is more than 2 letters, because 2 letters word normally impossible to change, and make the start word exist in the dictionary
      return word
    for char in word:                                             ##loop every character of the word
      if char.isdigit():                                          ##if the character is number then error message pop up
        print ("\nThe word you input cannot contain digit . Please try again...\n")
        break
    else:                                                         ##if too short or not in dictionary, error message also pop up
      print ("\nThe word you input does not exist in the dictionary or too short . Please try again...\n")

##function that check the target word to ensure that it is same length with start word/ non-digit/ word in the dictionary
def check_target_word(same_length_word,start):
  while True:
    word= input ("Enter target word: ").replace(" ","")         ##ignore the blank space that user enter
    if word == start:                                           ##to prevent the start word is the target word
      print("\nPlease select another words other than the start word. Please try again ...\n")
      continue
    if word in same_length_word:                            ##to ensure the target word is same length with start word and also exist in the dictionary
      return word
    for char in word:                                       ##loop every character of the word
      if char.isdigit():                                    ##if the character is number then error message pop up
        print ("\nThe word you input cannot contain digit . Please try again...\n")
        break
    else:                                                 ##if too short or too long not in dictionary, error message also pop up
      print("\nThe word you input should be same length with start word and in the dictionary. Please try again...\n")
      continue
def check_absent_word(start,target,same_length_word):         ## the list only be return when all word in the list is fully meet requirements
  correct=0                                                   ##use to end the loop
  while True:
    words = str(input("Enter the words you do not want to appear in the path (in the format of 'head, gold, sage'): ")).replace(" ", "").split(',')  ##change the string input into list
    for word in words:                                        ##iterate each word in the list
      if word == start or word == target:                     ##if the word is "start" and "target" word then error message pop up
        correct = 0
        print("\nThe words list you provided included start or target words. Please try again...\n")
        break
      elif word== "":
        return []
      elif word in same_length_word:                      ## condition to check the word whether in the dictionary or not
        correct +=1
        if correct == len(words):                         ##if all words is fulfil requirement then list will be returned
          return words
        continue
      elif len(word)!= len(target) :                        ##if the length of word not equal to the length of  "start" or "target" word then error message pop up
        correct = 0
        print("\n *"+word +"* is not in same length with the start word and target word. Please try again...\n")
        break
      elif word not in same_length_word:                  ##if the word is not in the list then error message pop up
        correct = 0
        print("\n*" + word + "* is not in dictionary. Please try again...\n")
        break

##function that check intermediary word if it meet requirements
def check_intermediary_word(start, target,same_length_word, absent_words):

    while True:
      answer = input("Do you want a word that must exist on the path? (Ans: y/n) :").lower()      ##ask user if they want to prompt in the must- exist word
      if answer=="n":                                                                               ##if input "n" do nothing
        return False
      elif answer=="y":                                                                             ##if input "y" ask the user enter the word
        while True:
          word= input("Please enter the word you want (or enter 'z' to not provide must-exist word): ").lower()
          if word== start or word == target:                                                      ##if the must-exist word equal to start or target word, user need to enter again
            print ("\nIt cannot be same as start word or target word. Please try again...\n")
          elif word == "z":                                                                         ##user can cancel the loop if they do not want to enter must-exist word
            return False
          elif len(word) != len(start):                                                               ##check the length of must-exist word whether it same length with start word or not
            print ("\nIt should same length with start and target word. Please try again...\n")
          elif word not in same_length_word or word in absent_words:                              ## check if the must-exist word present in absent word list or dictionary
            print("\nIt is not in dictionary or exist in absent words list. Please try again...\n")
          else:
            return word
      else:                                                           ##error message if user prompt in other character such as "m", "s"....
        print("\nYou only can input either 'n' or 'y' . Please try again.....\n")

## function that will return word that is one word different from specific word
## eg. specific word= hide
## 'aide', 'bide', 'eide', 'nide', 'ride', 'side', 'tide', 'vide', 'wide', 'hade', 'hike', 'hire', 'hive' will be returned and added into list
def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and          ##condition which restrict a seen word append to the list
                    word not in list]

##function that return one letter different word list from the specific word
def find(specific_word, same_length_words, seen):
  combination_list = []
  for i in range(len(specific_word)):
    combination_list += build(specific_word[:i] + "." + specific_word[i + 1:], same_length_words, seen, combination_list) ## "." as wildcard which can match everything
  return combination_list


##using "breadth first searching method to find the shortest path to target word.
##add current possible path into queue and pop the first item in queue to check if it reach the target word
def bfs(start,target,same_length_words,seen):
    queue= deque([[start]])                 ## implement deque so I can use popleft function
    while queue:
      path= queue.popleft()                 ##path variable to store the how the "start" word reach "target" word, popleft () keep the queue in decremental state to prevent infinite loop
      possible_target_word= path[-1]        ##possible target word is the last item in the list , if hide is "start", seek is "target" eg. ['hide', 'hire', 'sire', 'sirs', 'sers', 'sees', 'seek'] <- seek will be the last item in the list
      if possible_target_word== target:     ##if the possible target word is the "target" word then return the path
        return path
      for item in find(possible_target_word,same_length_words,seen):  ##loop in the combination list by using the function above *find()*
        queue.append(path+[item])           ## queue will be like ['hide','aide'], ['hide','bide'], ['hide','eide']....
        seen[item]=True                     ## to ensure these appended word is marked as seen to prevent to add the same word into combination list again
    return False                            ##if the queue is empty then return false




###################################################______________MAIN PROGRAM_______________________________#########################################################################
file = file_check_error()                 ##check the entered file name if it is valid within the directory
content= file.read()                      ##read the file
words = content.splitlines()              ##Split a string into a list where each line is a list item
file.close()                              ##close the file
while True:
  start = check_start_word(words).lower()       ##let user input the start word and check if it meet requirements or not
  same_length_words=[]
  for word in words:                            ##loop every item in the dictionary
    if len(word)== len(start):                  ##every word which has same letter length with the"start" word will be appended into a list
      same_length_words.append(word)
  target= check_target_word(same_length_words,start).lower()      ##let user input the target word and check if it meet requirements or not
  break
absent_words= check_absent_word(start,target,same_length_words)   ##let user input the words they do not want it to appear in the path
seen = {start : True}                                         ##a set to store every seen words
intermediate_word= check_intermediary_word(start, target,same_length_words,absent_words)
for word in absent_words:                                   ##add absent words list into seen therefore it will not add into possible changing word list
  seen[word]= True
seen_for_second_route= seen.copy()                          ##a copy of seen set just in case of intermediate word exist


if intermediate_word:                                   ##if user enter intermediate word
  seen[target]=True                                       ##set the target word as seen to prevent it appear in the path from start to intermediate word
  result=bfs(start,intermediate_word,same_length_words,seen)               ##run the breadth first search from start word to intermediate word
  if result:                                                            ##check if there is possible path
    for word in result:
      seen_for_second_route[word]= True                                 ##mark all the word in first route path as seen to prevent the same word appear again
    result2= bfs(intermediate_word,target,same_length_words,seen_for_second_route)    ##run the second time breadth first search from intermediate word to target word
    if result2:                                                         ##check if there is possible path
      final_result= result+result2[1:]                                  ##combine two path
      print (len(final_result)-1,final_result)                          ## print out the final result
    else:
      print ("No path found")                             ##error message for second bfs
  else:
    print ("No path found")                      ##error message for first bfs

else:
  answer = input ('Do you want unique path (Ans: y/n) :').lower()
  if answer== "y":
    fhandler = open("common.txt")
    content= fhandler.read()                      ##read the file
    words = content.splitlines()
    for word in words:
      for word2 in same_length_words:
        if word ==word2:
          same_length_words.remove(word)
    result= bfs(start,target,same_length_words,seen)
    if result:                                                    ##if the path is returned and it will be true then print the result
      print (len(result)-1,result)
    else:                                                         ## if there is no path and the function will return false
      print ("No path found")

  elif answer =="n":
    result=bfs(start,target,same_length_words,seen)               ##store path list
    if result:                                                    ##if the path is returned and it will be true then print the result
      print (len(result)-1,result)
    else:                                                         ## if there is no path and the function will return false
      print ("No path found")




